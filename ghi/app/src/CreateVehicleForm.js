import React, { useState, useEffect } from "react";


function CreateVehicleForm() {
  const [name, setName] = useState("");
  const [picture_url, setPictureURL] = useState("");
  const [manufacturers, setManufacturers] = useState([]);
  const [manufacturer, setManufacturer] = useState("");

  const handleName = (event) => {
    const value = event.target.value;
    setName(value);
  };
  const handlePicture = (event) => {
    const value = event.target.value;
    setPictureURL(value);
  };
  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  };
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.picture_url = picture_url;
    data.manufacturer_id = manufacturer;

    const url = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setName("");
      setPictureURL("");
      setManufacturer("");
    }
  };
  const fetchData = async () => {
    const url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a vehicle model</h1>
          <form onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input
                value={name}
                onChange={handleName}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Model name...</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={picture_url}
                onChange={handlePicture}
                placeholder="Name"
                required
                type="url"
                name="picture_url"
                id="picture_url"
                className="form-control"
              />
              <label htmlFor="name">Picture URL...</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleManufacturerChange}
                value={manufacturer}
                required
                id="manufacturer"
                name="manufacturer"
                className="form-select">
                <option>Select a Manufacturer</option>
                {manufacturers.map((manufacturer) => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>
                      {manufacturer.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default CreateVehicleForm;
