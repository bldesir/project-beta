import React, { useState } from "react";


function CustomerForm() {
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [phone_number, setPhoneNumber] = useState("");
  const [address, setAddress] = useState("");

  const handleSubmit = async (event) => {
    const data = {};
    data.first_name = first_name;
    data.last_name = last_name;
    data.phone_number = phone_number;
    data.address = address;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(
      "http://localhost:8090/api/customers/",
      fetchConfig
    );

    if (response.ok) {
      setFirstName("");
      setLastName("");
      setPhoneNumber("");
      setAddress("");
    }
  };

  const handleFirstNameChange = async (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const handleLastNameChange = async (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const handlePhoneNumberChange = async (event) => {
    const value = event.target.value;
    setPhoneNumber(value);
  };

  const handleAddressChange = async (event) => {
    const value = event.target.value;
    setAddress(value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Customer</h1>
          <form onSubmit={handleSubmit} id="add-salesperson-form">
            <div className="form-floating mb-3">
              <input
                value={first_name}
                onChange={handleFirstNameChange}
                placeholder="First Name"
                required
                type="text"
                name="first_name"
                id="first_name"
                className="form-control"/>
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={last_name}
                onChange={handleLastNameChange}
                placeholder="Last Name"
                required
                type="text"
                name="last_name"
                id="last_name"
                className="form-control"/>
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={phone_number}
                onChange={handlePhoneNumberChange}
                placeholder="Phone Number"
                required
                type="tel"
                name="phone_number"
                id="phone_number"
                className="form-control"/>
              <label htmlFor="phone_number">Phone Number</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={address}
                onChange={handleAddressChange}
                placeholder="Address"
                required
                type="text"
                name="address"
                id="address"
                className="form-control"/>
              <label htmlFor="address">Address</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CustomerForm;
