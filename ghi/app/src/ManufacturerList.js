function ManufacturerList(props) {
  const { manufacturers } = props;
  return (
    <div className="shadow p-4 mt-4">
      <h1>Manufacturers</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.manufacturers.map(manufacturer => {
            return (
              <tr key={manufacturer.href}>
                <td>{ manufacturer.name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )
}


export default ManufacturerList;
