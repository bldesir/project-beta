import React, { useState, useEffect } from "react";


function ServiceForm() {
  const [vin, setVin] = useState("");
  const [customer, setCustomer] = useState("");
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [reason, setReason] = useState("");
  const [technicians, setTechnicians] = useState([]);
  const [technician, setTechnician] = useState("");

  const handleVin = (event) => {
    const value = event.target.value;
    setVin(value);
  };
  const handleCustomer = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };
  const handleDate = (event) => {
    const value = event.target.value;
    setDate(value);
  };
  const handleTime = (event) => {
    const value = event.target.value;
    setTime(value);
  };
  const handleReason = (event) => {
    const value = event.target.value;
    setReason(value);
  };
  const handleTechnicianChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
  };
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.vin = vin;
    data.customer = customer;
    data.reason = reason;
    data.date = date;
    data.time = time;
    data.technician = technician;

    const url = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setVin("");
      setCustomer("");
      setDate("");
      setTime("");
      setReason("");
      setTechnician("");
    }
  };
  const fetchData = async () => {
    const url = "http://localhost:8080/api/technicians/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a service appointment</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">
            <p style={{ marginBottom: "0px" }}>Automobile VIN</p>
            <div className="form-floating mb-3">
              <input
                value={vin}
                onChange={handleVin}
                placeholder="Name"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
            </div>
            <p style={{ marginBottom: "0px" }}>Customer</p>
            <div className="form-floating mb-3">
              <input
                value={customer}
                onChange={handleCustomer}
                placeholder="Name"
                required
                type="text"
                name="customer"
                id="customer"
                className="form-control"
              />
            </div>
            <p style={{ marginBottom: "0px" }}>Date</p>
            <div className="form-floating mb-3">
              <input
                value={date}
                onChange={handleDate}
                placeholder="Name"
                required
                type="date"
                name="date"
                id="date"
                className="form-control"
              />
            </div>
            <p style={{ marginBottom: "0px" }}>Time</p>
            <div className="form-floating mb-3">
              <input
                value={time}
                onChange={handleTime}
                placeholder="Name"
                required
                type="time"
                name="time"
                id="time"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <select
                onChange={handleTechnicianChange}
                value={technician}
                required
                id="technician"
                name="technician"
                className="form-select"
              >
                <option>Select a Technician</option>
                {technicians.map((technician) => {
                  return (
                    <option key={technician.id} value={technician.id}>
                      {technician.first_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <p style={{ marginBottom: "0px" }}>Reason</p>
            <div className="form-floating mb-3">
              <input
                value={reason}
                onChange={handleReason}
                placeholder="Name"
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
              />
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default ServiceForm;
