import json
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Sale, Customer, Salesperson, AutomobileVO
from .encoders import SaleDetailEncoder, CustomerDetailEncoder
from .encoders import SalespersonDetailEncoder, DecimalEncoder
from .encoders  import AutomobileVODetailEncoder


@require_http_methods(['GET', 'DELETE'])
def api_show_sale(request, pk):
  if request.method == 'GET':
    try:
      sale = Sale.objects.get(id=pk)
      return JsonResponse(
        sale,
        encoder=SaleDetailEncoder,
        safe=False
      )
    except Sale.DoesNotExist:
      return JsonResponse(
        {'message': 'this sale does exist'},
        status=404
      )
  else:
    count, _ = Sale.objects.filter(id=pk).delete()
    return JsonResponse(
      {'deleted': count > 0}
    )


@require_http_methods(['GET', 'POST'])
def api_list_sales(request):
  if request.method == 'GET':
    sales = Sale.objects.all()
    if len(sales) == 0:
      return JsonResponse(
        {'message': 'there aren\'t any sales'},
        status=404
      )
    else:
      return JsonResponse(
        sales,
        encoder=SaleDetailEncoder,
        safe=False
      )
  else:
    content = json.loads(request.body)
    print(content)
    try:
      automobile_vin = content['automobile']['vin']
      automobile = AutomobileVO.objects.get(vin=automobile_vin)
      content['automobile'] = automobile
    except (AutomobileVO.DoesNotExist) as e:
      return JsonResponse(
        {'message': 'the automobile does not exist'},
        status = 404
      )
    try:
      salesperson_id = content['salesperson']['employee_id']
      salesperson = Salesperson.objects.get(employee_id=salesperson_id)
      content['salesperson'] = salesperson
    except Salesperson.DoesNotExist:
      return JsonResponse(
        {'message': 'the salesperson does not exist'},
        status = 404
      )
    try:
      customer_phone_number = content['customer']['phone_number']
      customer = Customer.objects.get(phone_number=customer_phone_number)
      content['customer'] = customer
    except Customer.DoesNotExist:
      return JsonResponse(
        {'message': 'the customer does not exist'},
        status = 404
      )
    try:
      sale = Sale.objects.get(automobile=automobile)
      return JsonResponse(
        {'message': f'this automobile has already been sold to {sale.customer}'},
        status = 400
      )
    except Sale.DoesNotExist:
      pass
    sale = Sale.objects.create(**content)
    sale.sell()
    return JsonResponse(
      sale,
      encoder=SaleDetailEncoder,
      safe=False
    )


@require_http_methods(['GET', 'POST'])
def api_list_customers(request):
  if request.method == 'GET':
    customers = Customer.objects.all()
    if len(customers) == 0:
      return JsonResponse(
        {'message': 'there aren\'t any customers'},
        status=404
      )
    else:
      return JsonResponse(
        customers,
        encoder=CustomerDetailEncoder,
        safe=False
      )
  else:
    content = json.loads(request.body)
    customer = Customer.objects.create(**content)
    return JsonResponse(
      customer,
      encoder=CustomerDetailEncoder,
      safe=False
    )

@require_http_methods(['DELETE'])
def api_show_customer(request, pk):
  count, _ = Customer.objects.filter(id=pk).delete()
  return JsonResponse(
    {'deleted': count > 0}
  )

@require_http_methods(['GET', 'POST'])
def api_list_salespeople(request):
  if request.method == 'GET':
    salespeople = Salesperson.objects.all()
    if len(salespeople) == 0:
      return JsonResponse(
        {'message': 'there aren\'t any salespeople'},
        status=404
      )
    else:
      return JsonResponse(
        salespeople,
        encoder=SalespersonDetailEncoder,
        safe=False
      )
  else:
    content = json.loads(request.body)
    salesperson = Salesperson.objects.create(**content)
    return JsonResponse(
      salesperson,
      encoder=SalespersonDetailEncoder,
      safe=False
    )


@require_http_methods(['DELETE'])
def api_show_salesperson(request, pk):
  count, _ = Salesperson.objects.filter(id=pk).delete()
  return JsonResponse(
    {'deleted': count > 0}
  )
